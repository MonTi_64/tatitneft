import urllib.request
import os


def get_file(url_link: str, html_files_folder: str) -> None:
    # check if not empty (obviously needs proper validation for URL)
    if not url_link:
        # place to add URL validation
        print(f"No URL is provided")
        return

    if not html_files_folder:
        # place to add folder path validation
        print(f"Please provide a file folder!")
        return

    try:
        # get website name from URL
        file_name = url_link.split('//')[1]

        # get file and save it / old method but works for our case, just adhering to KISS
        urllib.request.urlretrieve(url_link, f'{html_files_folder}/{file_name}.html')
    except:
        print(f"Error happened during URL {url_link} download!")


if __name__ == '__main__':

    urls = ['https://www.google.com', 'https://www.yahoo.com']

    html_folder = 'html_files'

    # get the current working directory
    directory = os.getcwd()

    # make dir for files
    os.makedirs(f'{directory}/{html_folder}', exist_ok=True)

    # iterate over URL
    for url in urls:
        # get it and save it
        get_file(url, html_folder)

