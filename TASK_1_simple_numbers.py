from typing import List, Optional


def calc_simple_numbers(n: int) -> Optional[List[int]]:
    """
    time complexity: O(N^2)
    space complexity: O(1) - if we don't count the output arr
    :param n:
    :return:
    """

    # result arr
    result = []

    # input validation
    if not type(n) == int or not n > 0:
        print("Please provide valid integer to input!")
        return

    # iterate till last number including it
    for num in range(2, n + 1):
        # iterate over possible dividers
        for possible_divider in range(2, num):
            # divider is found break
            if num % possible_divider == 0:
                break
        # no dividers found for this num - append to result
        else:
            result.append(num)

    return result


if __name__ == '__main__':
    print(calc_simple_numbers(50))
