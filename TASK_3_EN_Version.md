# Task 3

## Requirements

### Functional

client side:

- get_azs_list() - get list of fuel stations

admin side:

- add an icon for each fuel type
- add an icon for each service type
- modify types of field and services
- add new fuel stations
- add new types of fuel
- add new type of services

### Non-functional

- number of fuel type ~ 10
- number of services for stations ~ 100
- number of station ~ 10000

## Entities structure

We have few entities:
- users
- fuels stations
- fuel types
- services

Let's consider what kind of fields entities can have:

USER:

    {
        id: string,
        phone: string,
        name: string,
        role: 'CLIENT' | 'ADMIN' | 'BLOCKED'
        props: json
    }


FUEL_STATION:

    {
        id: number,
        coordinates: string,
        number: string,
        address: string,
        list_of_images: string[]
        list_of_services: SERVICE[]
        list_of_fuel_prices: FUEL_PRICE[]
    }

SERVICE: 

    {
        name: string,
        description: string,
        price: string
        icon: string
    }

FUEL_PRICES: 

    {
        name: string,
        currency: string,
        price: string
        icon: string
    }

We can use following relationships in DB:
- FUEL_STATION -> SERVICE: 1 to many
- FUEL_STATION -> FUEL_PRICE: 1 to many
- FUEL_STATION -> IMAGES: 1 to many

FUEL_STATIONS table:

    id: INT(10),
    coordinates: VARCHAR(100),
    number: VARCHAR(100), # or INT not sure about the type here
    address: VARCHAR(100)
    props: JSON // for metadata

SERVICE table: 

    {
        
        name: VARCHAR(100)
        description: VARCHAR(200)
        price: VARCHAR(100)
        icon: VARCHAR(100) # if icon is a URL. Alternativly we can store icons in separate table in DB, then this would FK
        fuel_station_id: INT(10), # FK 
        props: JSON // for metadata
    }

FUEL_PRICES table: 

    {
        
        name: VARCHAR(100)
        description: VARCHAR(200)
        price: VARCHAR(100)
        icon: VARCHAR(100) # if icon is a URL. Alternativly we can store icons in separate table in DB, then this would FK
        fuel_station_id: INT(10), # FK 
        props: JSON // for metadata
    }

Possibly we have separate images table if we want to keep track of individual images and / or icons.
That depends on how we want our system to work. We might want to prefer which images can be displayed per each
fuel station or modify some metadata (name, description etc), so we add it in our design.

IMAGES table:

    {
        id: INT(10)
        url: VARCHAR(100)
        fuel_station_id: INT(10), # FK 
        props: JSON // for metadata
    }

## API

### Client side

On client side users have UI with certain APIs:

Lets describe it:

Get paginated list of fuel stations:
URL: `/azs?limit=10&offset=0 GET`

RETURNS:

    {
        result: FUEL_STATION_BASIC[],
        message: {
            type: 'success' | 'error',
            text: ''
        }
    }

    FUEL_STATION_BASIC
        {
            id: string,
            coordinates: string,
            number: string,
            address: string,
        }

Get specific fuel station
URL: `/azs?id={azs_id} GET`

RETURNS:

    {
        result: FUEL_STATION_FULL,
        message: {
            type: 'success' | 'error',
            text: ''
        }
    }

    FUEL_STATION_FULL 
        {
            id: string,
            coordinates: string,
            number: string,
            address: string,
            list_of_images: string[]
            list_of_services: SERVICE[]
            list_of_fuel_prices: FUEL_PRICE[]
        }

Admin side:

We have full CRUD over entities in our system. Method that we need:

- `GET /admin/entities?limit=10&offset=0` - get multiple entities

RESPONSE:
``` 
    {
        result: ENTITY[],
        message: {
            type: 'success' | 'error',
            text: ''
        }
    }
```

- `GET /admin/entity?id=1` - get single entity

RESPONSE:
``` 
    {
        result: ENTITY,
        message: {
            type: 'success' | 'error',
            text: ''
        }
    }
```

- `POST /admin/entity` , entity in request body

RESPONSE:
``` 
    {
        result: ENTITY, # newly created entity
        message: {
            type: 'success' | 'error',
            text: 'Entity created successfully' | 'An error occurred while adding an entry'
        }
    }
```

- `PATCH /admin/entity` , updates in request body


RESPONSE:
``` 
    {
        result: ENTITY, # updated entity
        message: {
            type: 'success' | 'error',
            text: 'Entity updated successfully' | 'An error occurred while updating an entry'
        }
    }
```

- `PUT /admin/entity` , entity in request body

RESPONSE:
``` 
    {
        result: ENTITY, # updated entity
        message: {
            type: 'success' | 'error',
            text: 'Entity updated successfully' | 'An error occurred while updating an entry'
        }
    }
```


- `DELETE /admin/entity`

RESPONSE:
``` 
    {
        message: {
            type: 'success' | 'error',
            text: 'Entity deleted successfully' | 'An error occurred while deleting an entry'
        }
    }
```

## Service interaction

Link to the design scheme:
https://drive.google.com/file/d/1Kri_39w_wgXoieQI3glgu3mBmiErwstX/view?usp=sharing

Components that we require:

- There should a service that updates information about fuel stations from sources on regular basis
  - can be done on Python with aiocron library
  - has a list of sources, periodically sends request to sources and gets data
  - then based on this data it updates entities in RDMS
  - it can do it directly by interacting with RDMS or by using our API for admin access


- We store the data about our entities in RDMS like MySQL or Postgres
  - we should consider replication for availability purposes
  - in future, we might consider adding sharding by fuel station id / number in case our system needs to scale


- Redis as caching solution
  - we should reset the cache based on command from service that periodically updates info from sources
  - since they are not updated as often we practically can cache everything - fuel stations, fuel types, fuel services
  - we might consider having a standby replication instance as well for availability


- as LB solution / API gateway we can use nginx


- We should have servers that serve API to our users
  - should be stateless for scaling
  - we can use Python with framework of our choice (FastAPI, aiohttp etc), for interacting with RDMS we can use sql alchemy


- Additionally, there are API servers that provide admin interface
  - two instances will be enough, since it will not be loaded as much
  - we can use Python with framework of our choice (FastAPI, aiohttp etc), for interacting with RDMS we can use sql alchemy



- For icon storage we might use S3 solution provided by cloud operators, like Yandex
  - if we want on premise solution, we can use something like minio


- We probably would want a robust solution for metric and logging collection
  - It's critical to monitor performance of updater service and watch out for possible errors
  - we can use something like loki or elastic stack


- For authentication, we can create JWT tokens when users log in the system and attach them to cookies.
  - For a more
  complex solution we might create a separate service responsible for authentication. We might delegate token validation to nginx, using lua plugins
  - In our design each user has a role. Users with a `Client` role can access API endpoints for working with fuel stations.
  - The users with `Admin` role can access any part of the system
  - The `Blocked` users can not interact with the system

## Workflows

### Client

Client interacts with 2 endpoints: `/azs?limit=10&offset=0 GET` and `/azs?id={azs_id} GET`.
First client retrieves the paginated list of all AZS in our system. Then when he wants to see information
about some specific AZS he uses second endpoint by passing `azs_iz` to it. He gets full information including images, services and fuel prices.

In our case we have included full information in second handler. Obviously we can split this information by separate handlers.
It depends on how we want our UI to interact with API. In that case we split second handler into multiple handlers for each entity
, like `get_services`, `get_fuel_prices`, `get_images` etc.

### Admin

Let consider the cases we need to cover:

- add an icon for each fuel type

If we want to host our own icons, we can use s3 compatible cloud storage. We can create separate buckets for 
each entity type (Services bucket, Fuel types buckets etc). To change icon type admin uses admin UI, where he 
has CRUD access to the fuel type table. He uses `PATCH` request to update URL to the icon.

- add an icon for each service type

Same as above. Admin can upload an icon to storage solution. Then he updates record for service type in admin UI.

- modify types of field and services

Admin access admin UI and uses CRUD `PATCH` or `PUT` to modify records.

- add new fuel stations

Admin access admin UI and uses CRUD `POST` to add a record.

- add new types of fuel

Admin access admin UI and uses CRUD `POST` to add a record.

- add new type of services

Admin access admin UI and uses CRUD `POST` to add a record.

## Notes

We need to pay attention on how the updater service synchronizes the data. Let's consider few cases:

- For example what if certain AZS were deleted? We can allow to delete them in DB, or we can 'mark them as deleted'.
It depends on how we want to interact with data.

- There is an option to treat all updates from external sources as one time use. That means when new update happens 
we discard old records completely (we might store them in some analytical DB for later processing) and insert new ones.

- What if there is a case when we manually created an AZS, but then there are update from external services that uses same AZS id or name.
To solve this issue we can introduce `type` column to our records, for example like `type = 'Manual'` (for records created by admins, which we want to persist no matter what happens) and `type = 'From_external_sources'`(which can be overwritten by updater service)
and allows updater service interact only with entities with type `From_external_sources`.

